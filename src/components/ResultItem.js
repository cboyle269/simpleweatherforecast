import React, { Component } from 'react'

export class ResultItem extends Component {

  forcast = this.props.forecast;

  render() {

    return (
      <li className="forcastDay">
        <h2>{this.forcast.date}</h2>
            <img src={this.forcast.day.condition.icon} alt="icon" className="forecastIcon"></img>
            <h3>{this.forcast.day.condition.text}</h3>
            <h4>Average Temp: {this.forcast.day.avgtemp_c} &deg;C</h4>
      </li>
    )
  }
}

export default ResultItem
