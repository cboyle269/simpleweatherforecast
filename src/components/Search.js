import React, { Component } from 'react'

export class Search extends Component {
  state = {
      location: '',
      days: 1
  }  

  onChange = (e) =>{
    this.setState({
      [e.target.name]: e.target.value
    });    
  }

  onSubmit = (e) =>{
    e.preventDefault();
    this.props.preformSearch(this.state.location, this.state.days);  
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} className="searchForm">
        <input className="locationInput"
          type="text" 
          name="location"
          placeholder="Location"
          value={this.state.location}
          onChange={this.onChange}
        />

        <select className="daysInput" name="days" onChange={this.onChange}>
          <option value="1">1 Day</option>
          <option value="2">2 Days</option>
          <option value="3">3 Days</option>
          <option value="4">4 Days</option>
          <option value="5">5 Days</option>
        </select>
        
        <input 
          type="submit"
          value="Search"
          className="searchBtn"
        />

      </form>
    )
  }
}

export default Search;