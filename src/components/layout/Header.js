import React from 'react'

export default function Header() {
  return (
    <header className="headerComponent">
      <h1>Simple Weather Forecast</h1>
    </header>
  )
}
