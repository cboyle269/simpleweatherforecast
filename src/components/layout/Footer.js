import React from 'react'

export default function Footer() {
  return (
    <div style={{position:"flex"}}>
      <footer className="footerComponent">
        <p>Created by Connor Boyle <br/>
        Powered by <a href="https://www.apixu.com/" title="Weather API">Apixu.com</a>
        </p>
      </footer>
    </div>
  )
}
