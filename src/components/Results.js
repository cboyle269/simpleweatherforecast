import React, { Component } from 'react'
import ResultItem from './ResultItem'

export class Results extends Component {

  render() {
    return (
      <div style={{ display: this.props.searchPreformed ? 'block' : 'none' }}>
        <div className="forecastTitle">
          <h1>{this.props.forecasts.length} day forecast for {this.props.location}</h1>
        </div>
        <ul className="forecasts">
          {this.props.forecasts.map((forcast, i) => {
            return <ResultItem forecast={forcast} key={i} />
          }
          )}
        </ul>
      </div>

    )
  }
}


export default Results
