import React, { Component } from 'react';
import Header from './components/layout/Header'
import Footer from './components/layout/Footer'
import Search from './components/Search'
import Results from './components/Results'
import axios from 'axios'
import { apiKey } from './config'
import './App.css';

class App extends Component {

  state = {
    location: '',
    forecasts: [],
    searchPreformed: false
  }

   async preformSearch (location, days) {
    const request = `http://api.apixu.com/v1/forecast.json?key=${apiKey}&q=${location}&days=${days}`;
    console.log(request);
    const res = await axios.get(request);
    const response = res.data;
    this.setState({
      location: response.location.name,
      forecasts: response.forecast.forecastday,
      searchPreformed: true
    });

    console.log(this.state);

  }


  render() {
    return (
      <div className="App">
        <Header/>
        <Search preformSearch={this.preformSearch.bind(this)}/>
        <Results location={this.state.location} forecasts={this.state.forecasts} searchPreformed={this.state.searchPreformed}/>
        <Footer/>
      </div>
    );
  }
}

export default App;
